import { Component } from '@angular/core';
import { NavController, NavParams, Content, ModalController, Platform } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AboutPage } from '../about/about';
import { AshishPage } from '../ashish/ashish';
import { DatesPage } from '../dates/dates';
import { ContactPage } from '../contact/contact';
import { Http } from '@angular/http';
import { Insomnia } from '@ionic-native/insomnia';
import { NativeAudio } from '@ionic-native/native-audio';
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = AshishPage;
  tab4Root = DatesPage;
  tab5Root = ContactPage;

  constructor(
    private platform: Platform,
    private http: Http,
    private nativeAudio: NativeAudio,) {

        if (!this.platform.is('cordova')) {
            var audio = new Audio('assets/sounds/background_song.mp3');
            console.log(audio.duration);
            audio.loop = true;
            audio.play();
        }
  }

  
}
